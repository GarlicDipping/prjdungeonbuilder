﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 게임 내에서 턴이 흐르는 것을 총괄한다
/// </summary>
public class TurnController : MonoBehaviour
{
	public Dungeon dungeon;
	public static TurnController ins { get; private set; }
	// Use this for initialization
	void Awake ()
	{
		ins = this;
		timeCount = 0;
	}

	float timePerTurn = 1f;
	float timeCount = 0;
	// Update is called once per frame
	void Update ()
	{
		timeCount += Time.deltaTime;
		if(timeCount > timePerTurn)
		{
			timeCount -= timePerTurn;
			dungeon.OnTurn();
		}
	}
}

public class Dungeon
{
	public Dictionary<Team, List<TurnActor>> turnActors;
	public enum State
	{
		/// <summary>
		/// 영웅 파티가 없어서 던전이 쉬는 중임
		/// </summary>
		Rest,
		/// <summary>
		/// 영웅들이 던전을 탐색중이다.
		/// </summary>
		Heroes_Searching,
		/// <summary>
		/// 영웅들이 전투 방에 들어왔음. 전투중이다.
		/// </summary>
		Fighting,
		
	}
	public Team currentTurn { get; private set; }

	/// <summary>
	/// 영웅이 던전에 입장했다
	/// </summary>
	public void OnHeroEnter()
	{

	}

	/// <summary>
	/// 던전의 턴 카운트 타이밍이 되었음
	/// </summary>
	public void OnTurn()
	{
		foreach(var actor in turnActors[currentTurn])
		{
			actor.OnTurn(currentTurn);
		}
	}
}

/// <summary>
/// 던전 관리자 캐릭터
/// </summary>
public class DungeonManager
{
	public Skill managerSkill;
}