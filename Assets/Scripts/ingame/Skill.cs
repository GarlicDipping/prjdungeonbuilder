﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Skill
{
	public int order;
	/// <summary>
	/// 이 스킬을 쓰기 위해 필요한 턴 수
	/// </summary>
	public int cooltime;
	/// <summary>
	/// 스킬의 총 계수
	/// </summary>
	public float total_effectiveness;
	/// <summary>
	/// 이 스킬은 랜덤한 적을 3회 타격한다, 랜덤한 아군을 2회 회복시킨다, 기타등등
	/// </summary>
	public List<SkillAction> skillActions;
	public bool target_teammate;

	public void CastSkill(TurnActor caster)
	{
		foreach (var action in skillActions)
		{
			action.PerformAction(caster);
		}
	}
}

public class SkillCooltimeCounter
{
	public SkillCooltimeCounter(Skill skill)
	{
		cooltime_left = skill.cooltime;
	}

	public int cooltime_left { get; private set; }
	public int DecrementCooltime(int amount = 1)
	{
		cooltime_left -= amount;
		if (cooltime_left < 0) { cooltime_left = 0; }
		return cooltime_left;
	}

	public int IncrementCooltime(int amount = 1)
	{
		cooltime_left += amount;
		return cooltime_left;
	}
}