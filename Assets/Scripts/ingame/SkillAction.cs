﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum SkillActionType
{
	Attack,
	Heal,
}


public abstract class SkillAction
{
	public Skill parent;

	/// <summary>
	/// 이 액션의 실제로 적용될 계수 비중
	/// ex)total_eff = 3, eff_rate = 0.5면 1.5가 최종 계수
	/// </summary>
	public float effectiveness_rate;

	public abstract void PerformAction(TurnActor caster);
}

public class AttackSkillAction : SkillAction
{
	public override void PerformAction(TurnActor caster)
	{
		
	}
}
