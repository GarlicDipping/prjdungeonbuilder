﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Team
{
	/// <summary>
	/// 마왕군
	/// </summary>
	Player,
	/// <summary>
	/// 영웅들
	/// </summary>
	Heroes,
}

/// <summary>
/// 여러 턴 액터가 각종 스킬을 유저 마음대로 구성할 수 있는 걸 목표하고 있음
/// 특정 캐릭터는 무조건 xx를 가지고 있다던가 할 수도 있고...
/// json에 저장해두게 될 듯?
/// </summary>
public class TurnActorStat
{
	public int hp;
	public int mp;	
}

/// <summary>
/// 던전 인게임에서 보여지며 활용될 TurnActor 클래스
/// </summary>
public class TurnActor
{
	public TurnActorStat stat { get; private set; }
	public Team team;
	public int current_hp, current_mp;
	public Skill defaultSkill;
	public Skill[] activeSkills;
	public Dictionary<Skill, SkillCooltimeCounter> cooltimeDict { get; private set; }

	public TurnActor(TurnActorStat stat, Team team)
	{
		this.stat = stat;
		this.team = team;
		cooltimeDict = new Dictionary<Skill, SkillCooltimeCounter>();
		if(defaultSkill != null)
		{
			cooltimeDict.Add(defaultSkill, new SkillCooltimeCounter(defaultSkill));
		}
		foreach(var activeSkill in activeSkills)
		{
			if(activeSkill != null)
			{
				cooltimeDict.Add(activeSkill, new SkillCooltimeCounter(activeSkill));
			}
		}
		current_hp = stat.hp;
		current_mp = stat.mp;
	}

	List<Skill> currentValidActiveSkills = new List<Skill>();
	public virtual void OnTurn(Team currentTurnTeam)
	{
		findCurrentValidActiveSkill();
		if(currentValidActiveSkills.Count > 0)
		{
			currentValidActiveSkills[0].CastSkill(this);
		}
		else
		{
			//default skill 캐스팅
			if(defaultSkill != null)
			{
				defaultSkill.CastSkill(this);
			}
		}
	}

	void findCurrentValidActiveSkill()
	{
		currentValidActiveSkills.Clear();
		foreach (var skill in activeSkills)
		{
			if (skill == null)
			{
				continue;
			}
			else
			{
				if (cooltimeDict[skill].cooltime_left == 0)
				{
					currentValidActiveSkills.Add(skill);
				}
			}
		}
	}

	public virtual void OnHit(TurnActor attacker, int damage)
	{
		current_hp -= damage;
		if (current_hp <= 0)
		{
			current_hp = 0;
			OnDead();
		}
	}

	public void OnDead()
	{

	}
}
